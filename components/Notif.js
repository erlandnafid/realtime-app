import React from "react";

class Notif extends React.Component {
  state = { isNotifOpen: false };

  onClickNotif = () => {
    this.setState({ isNotifOpen: !this.state.isNotifOpen });
  };

  render() {
    return (
      <React.Fragment>
        <div className="notif" onClick={this.onClickNotif}>
          {this.props.notif.length}
        </div>
        {this.state.isNotifOpen ? (
          <div className="notif-body">
            {this.props.notif.map((e, i) => (
              <div key={i} className="notif-item">
                <h6 className="mb-0 mt-3">New message from {e.user}:</h6>
                <p className="message">{e.message}</p>
              </div>
            ))}
          </div>
        ) : null}

        <style jsx>{`
          .notif {
            background: red;
            color: white;
            padding: 8px 16px;
            border-radius: 30px;
            margin-right: 1.5rem !important;
            cursor: pointer;
          }
          .notif-body {
            box-shadow: 2px 2px 5px 2px #ccc;
            position: absolute;
            background: #ffffff;
            padding: 0px 10px 15px 10px;
            right: 1.5rem;
            top: 4.5rem;
            z-index: 1;
            border-radius: 0.25rem;
            border: 1px solid #dee2e6;
            max-height: 300px;
            overflow: scroll;
            width: 225px;
          }
          .notif-item {
            border-bottom: 1px solid rgb(153, 153, 153);
          }
          .message {
            color: gray;
            line-height: 15px;
            font-size: 13px;
            margin-top: 5px;
          }
        `}</style>
      </React.Fragment>
    );
  }
}

export default Notif;
