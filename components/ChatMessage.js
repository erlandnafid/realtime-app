import React from "react";

class ChatMessage extends React.Component {
  render() {
    const { position = "left", message } = this.props;
    const isRight = position.toLowerCase() === "right";
    const align = isRight ? "text-right" : "text-left";
    const justify = isRight ? "justify-content-end" : "justify-content-start";

    return (
      <div className={`w-100 my-1 d-flex ${justify}`}>
        <div className="chat-box">
          <span className={`d-block text-secondary chat ${align}`}>
            {message}
          </span>
        </div>

        <style jsx>{`
          .chat-box {
            max-width: 70%;
            flex-grow: 0;
            padding: 0.5rem;
            border-radius: 0.25rem;
            border: 1px solid #dee2e6;
            background: #f8f9fa;
          }
          .chat {
            font-weight: 500,
            line-height: 1.4,
            white-space: pre-wrap,
          }
        `}</style>
      </div>
    );
  }
}

export default ChatMessage;
