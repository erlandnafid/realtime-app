import React, { Component } from "react";
import Layout from "../components/Layout";
import ChatTwo from "../components/ChatTwo";

class NotifPage extends Component {
  state = { user: null };

  handleKeyUp = (event) => {
    if (event.keyCode === 13) {
      const user = event.target.value;
      this.setState({ user });
    }
  };

  render() {
    const { user } = this.state;

    return (
      <Layout pageTitle="Delegate">
        <main className="home">
          <div className="row main">
            <section className="col-md-8 main-section px-5">
              <div className="px-5 mx-5">
                <span
                  className="d-block w-100 h1 text-light"
                  style={{ marginTop: -50 }}
                >
                  {user ? (
                    <span>
                      <span style={{ color: "#999" }}>Hello!</span> {user}
                    </span>
                  ) : (
                    `Please enter your name`
                  )}
                </span>

                {!user && (
                  <input
                    type="text"
                    className="form-control mt-3 px-3 py-2 input-name"
                    onKeyUp={this.handleKeyUp}
                    autoComplete="off"
                  />
                )}
              </div>
            </section>

            <section className="col-md-4 chat-section px-0">
              {user && <ChatTwo activeUser={user} />}
            </section>
          </div>
        </main>
        <style jsx>{`
          .input-name {
            background: transparent;
            color: #999;
            border: 0;
            border-bottom: 1px solid #666;
            border-radius: 0;
            font-size: 3rem;
            font-weight: 500;
            box-shadow: none !important;
          }
          .home {
            position: absolute;
            height: 100%;
            background: #343a40;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
          }
          .main {
            position: absolute;
            height: 100%;
            width: 100%;
          }
          .main-section {
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            align-content: center;
          }
          .chat-section {
            position: relative;
            display: flex;
            height: 100%;
            align-items: flex-start;
            align-content: space-between;
            flex-wrap: wrap;
            background: #fff;
          }
        `}</style>
      </Layout>
    );
  }
}

export default () => <NotifPage />;
